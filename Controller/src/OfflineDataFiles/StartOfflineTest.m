%% Settings 
% Set dataName and time
dataName = '180330_113217_WE2_3D_Outputs_20_25'; 
t_start = 2573 + 480;  %450/0
t_end = 2573 + 530 ;    %500/t(end); 
saveNewDataFile = 1; 

%% Loading data
dataSetNamesList = {'LopesModelPos','LopesModelVel','PhaseIn_s','WE2OperEna','StateDiagram'};
load(dataName)
fs = 1000;
t_orig = data.rawdata(:,1)';
t_index = find(t_orig>t_start,1):find(t_orig>t_end,1); 
ts = 1/fs;
t = (0:length(t_index)-1)/fs; %Simulink requires time to start at t = 0


%% Getting the correct data
% DataPrep
index_total = 1;

for i_dataSetNames = 1:length(dataSetNamesList)
    % Get current dataSet Name
    dataSetName = dataSetNamesList{i_dataSetNames};
    [s,e] = finddata(dataSetName,data.map);
    if saveNewDataFile == 1
        eval([dataSetName,' = data.rawdata(t_index,s:e)'';']);
    end
    eval(['index_',dataSetName,' = index_total:index_total + e - s;']);
    eval(['index_total = index_',dataSetName,'(end)+1;']);
end

if saveNewDataFile == 1
% Filling the matrix
OfflineData = zeros(index_total,length(t)); 
OfflineData(1,:) = t; 
for i_dataSetNames = 1:length(dataSetNamesList)
    dataSetName = dataSetNamesList{i_dataSetNames};
    eval(['OfflineData(index_',dataSetName,'+1,:) = ',dataSetName,';']);
end
save('OfflineData','OfflineData');
end

%% Needed for simulation
index_total = index_total-1; 
