% close all; 

%% Options
load('WE2_3D_Outputs')
joint = 1; 
plotNameList = {'SpringPositionsOut','PosTorqueOut'}; % Insert 1 or more signals that you want to plot
figureNumList = [1,1]; %State in which figure these need to be plotted
scaleContentToOne = 1; %This option scales all content to (-1 - 1)
load('WE2_3D_Outputs_names')

%% Checks
if size(plotNameList,2) ~= size(figureNumList,2)
    error('PlotNameList and figureNumList should be the same size')
end

%TimeStamp
t = WE2_3D_Outputs(1,:); 

prevFigureNum = 0; %PreAllocating
for i_plotName = 1:length(plotNameList)
    % Opening correct figure
    figure(figureNumList(i_plotName)); hold on; 
    
    % Resetting
    if prevFigureNum ~= figureNumList(i_plotName)
        sizeSubplot = [];
    end
    prevFigureNum = figureNumList(i_plotName);
        
    % Getting current plotName
    plotName = plotNameList{i_plotName}; 
    
    % Finding PlotName Rows
    [s,e] = finddata(plotName,nameList);
    sizeSubplot = max([sizeSubplot ceil(sqrt(e-s+1))]); 
    dataSet = s+1:e+1;
    
    % Making Subplot
    for i_Plot = 1:e-s+1
        subplot(sizeSubplot,sizeSubplot,i_Plot); hold on
        curSignal = WE2_3D_Outputs(dataSet(i_Plot),:); 
        % Scaling
        if scaleContentToOne
            curSignal = curSignal/max(abs(curSignal));
        end
        
        plot(t,curSignal)
    end
end