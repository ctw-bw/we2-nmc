function [firstFound,lastFound] = finddata(searchName,fileName)
% This function is able to find the indixes corresponding to the seach name
% The filename can either consists of a Struct or a Filname

%% Check if matrix is given (which is wrong, struct should be defined) 
if isa(fileName,'double')
    error('Struct/CellStruct should be given, not Value matrix')
end   

if isa(fileName,'struct')
    % In case of Struct
    searchdata = fileName;
elseif isa(fileName,'cell')
    searchdata.data = fileName; 
    searchdata.signalNames = fileName; 
else
    % In case of file, without .mat convention
    if exist(fileName,'file') == 0
        fileName = [fileName,'.mat'];
    end
    
    if exist(fileName,'file') == 0
        % In case no file and no struct are detected with that name
        error('No file detected with that name')
    elseif exist(fileName,'file') ==2
        % In case of a (found) file        
        load(fileName);             %Loading the file
        matObj = matfile(fileName); %Getting the struct name
        filespec = whos(matObj);
        eval(['searchdata = ',filespec.name,';']);
    end
end

%% Searching for input data 
saveFound =[0 0];
for i_names = 1:size(searchdata.data,2)
    temp_name = char(searchdata.signalNames(i_names));
    if strfind(temp_name,searchName)~=0
        if saveFound(1) == 0
            saveFound(1:2) = i_names;     % First Found Data Point
        else
            saveFound(2) = i_names;     % Last Found Data Point
        end
    end
end

if saveFound(1) == 0 
    % Wrong Input Data Name
    error('No data found')
else
    firstFound = saveFound(1);
    lastFound = saveFound(2);
end