load('TestingOffline')
close all
t = ans.Time' ; 
temp = ans.Data'; 

torque = temp(1:8,:);
angles = temp(9:16,:);
estimatedPhase = temp(17,:);
curstate = temp(18,:); 
anglevelocity = temp(19:26,:); 
variableTunnel = temp(27,:); 


num = 2; 
figure; 
subplot(2,2,1)
plot(t,torque(num,:)); hold on; plot(t,torque(num+4,:))
title(['Torque (N), Joint #',num2str(num)])
subplot(2,2,2) 
plot(t,angles(num,:)); hold on; plot(t,angles(num+4,:))
title(['Angle (rad), Joint #',num2str(num)])
subplot(2,2,3)
plot(t,anglevelocity(num,:)); hold on; plot(t,anglevelocity(num+4,:))
title(['AngleVel (rad/s), Joint #',num2str(num)])

figure; 
subplot(2,2,1)
plot(t,curstate)
title('State')
subplot(2,2,2)
plot(t,variableTunnel);
title('VariableTunnel')
subplot(2,2,3)
plot(t,estimatedPhase);
title('Estimated Phase')

%% There is something wrong with propogating the SetState
%% MAYOR BUG, NEEDS TO BE FIXED IN ALL LIBRARIES