%
% setCtrlPar.m
%   set control parameters
% 
%   given: param
%
%   set:
%       CtrlPar_theta0
%       CtrlPar_FP
%       CtrlPar_LL
%       CtrlPar_St
%       CtrlPar_Sw
%       CtrlPar_Tr
%       CtrlPar_FP_R
%       CtrlPar_St_R
%       CtrlPar_Sw_R
%       CtrlPar_Tr_R
%
% by Seungmoon Song
% Jan 2014
%

l_BlP = 1;
l_FPP = 3;
l_LLP = 1;
l_StP = 38;  %changed for Ankle
l_SwP = 27;
l_TrP = 2;

l_2DCtrl    = l_BlP + l_FPP + l_LLP + l_StP + l_SwP + l_TrP;
l_3DCtrl    = 18;

param2D     = param(1                       : l_2DCtrl);
param3D     = param(l_2DCtrl+1              : l_2DCtrl+l_3DCtrl);


% ==================================== %
% SET LATERAL PLANE CONTROL PARAMETERS %
% ==================================== %

% --------------------
% higher layer control
% --------------------

% foot placement Ctrl %
R_hipR0             = param3D(1)        *(-1)   *pi/180; 
R_hipR_cd           = param3D(2)        *10     *pi/180;    % [rad/m]
R_hipR_cv           = param3D(3)        *10     *pi/180;    % [rad*s/m]

CtrlPar_FP_R = [R_hipR0 R_hipR_cd R_hipR_cv];


% --------------
% stance control
% --------------

% 0: prestimulations
St_PreS_HAB       	= param3D(4)       *0.01;
St_PreS_HAD         = param3D(5)       *0.01;

% M1: realize compliant leg
R_St_FGain_HAB    	= param3D(6)        *.45/FmaxHAB;

% M3: balance trunk
R_St_GainTh_HAB     = param3D(7)        *2.4;
R_St_GainDTh_HAB    = param3D(8)        *.4;
R_St_GainTh_HAD     = param3D(9)        *.5;
R_St_GainDTh_HAD    = param3D(10)      	*.55;

% M4: compensate swing leg
R_St_SGainCHAB_HAB  = param3D(11)    	*1.5;
R_St_SGainCHAD_HAD  = param3D(12)      	*.4;

R_StPreStim = [St_PreS_HAB St_PreS_HAD];
R_St_M1     = [R_St_FGain_HAB];
R_St_M3     = [R_St_GainTh_HAB R_St_GainDTh_HAB R_St_GainTh_HAD R_St_GainDTh_HAD];
R_St_M4     = [R_St_SGainCHAB_HAB R_St_SGainCHAD_HAD];

CtrlPar_St_R = [R_StPreStim R_St_M1 R_St_M3 R_St_M4];

% indexes of control parameters
R_St_i = 0;
R_parSt_PreS_i  = R_St_i + 1	: R_St_i + length(R_StPreStim);
R_St_i = R_St_i + length(R_StPreStim);
R_parSt_M1_i  	= R_St_i + 1	: R_St_i + length(R_St_M1);
R_St_i = R_St_i + length(R_St_M1);
R_parSt_M3_i  	= R_St_i + 1	: R_St_i + length(R_St_M3);
R_St_i = R_St_i + length(R_St_M3);
R_parSt_M4_i  	= R_St_i + 1	: R_St_i + length(R_St_M4);
R_St_i = R_St_i + length(R_St_M4); % St_i is used in the Simulink file


% -------------
% swing control
% -------------

% constants (measurment parameters)
R_Sw_a2lopt_HAB     = 0.5442*rHAB*rhoHAB/loptHAB;
R_Sw_aRef_HAB       = 0.8628*(pi/2 - phirefHAB);
R_Sw_a2lopt_HAD     = 0.6798*rHAD*rhoHAD/loptHAD;
R_Sw_aRef_HAD       = 0.8053*(pi/2 - phirefHAD);

% 0: prestimulations
Sw_PreS_HAB       	= param3D(13)       *0.01;
Sw_PreS_HAD         = param3D(14)       *0.01;

% M6: swing hip
R_Sw_LGain_HAB      = param3D(15)       *1/R_Sw_a2lopt_HAB;
R_Sw_LGain_HAD      = param3D(16)       *1/R_Sw_a2lopt_HAD;

R_SwPreStim = [Sw_PreS_HAB Sw_PreS_HAD];
R_Sw_M6     = [R_Sw_a2lopt_HAB R_Sw_aRef_HAB R_Sw_LGain_HAB R_Sw_a2lopt_HAD R_Sw_aRef_HAD R_Sw_LGain_HAD];

CtrlPar_Sw_R = [R_SwPreStim R_Sw_M6];

% indexes of control parameters
R_Sw_i = 0;
R_parSw_PreS_i  = R_Sw_i + 1	: R_Sw_i + length(R_SwPreStim);
R_Sw_i = R_Sw_i + length(R_SwPreStim);
R_parSw_M6_i  	= R_Sw_i + 1	: R_Sw_i + length(R_Sw_M6);
R_Sw_i = R_Sw_i + length(R_Sw_M6); % St_i is used in the Simulink file


% ----------------------------------
% stance -> swing transition control
% ----------------------------------

R_Tr_St_sup         = param3D(17)       *1;       
R_Tr_Sw             = param3D(18)       *1;

CtrlPar_Tr_R = [R_Tr_St_sup R_Tr_Sw];


