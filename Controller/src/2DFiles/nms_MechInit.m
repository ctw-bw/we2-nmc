% 
% nms_MechInit.m
%   set mechanical parameters of the model
%
% set:
%   segment dynamics
%   muscle-skeleton attachments
%   muscle dynamics
%   ground contact dynamics
%   neural transmission delays
%
% modified from Hartmut Geyer
% October 2006
%
% by Seungmoon Song
% Jan 2014

% -----------------
% soft joint limits
% -----------------

phi34R_LowLimit  = -15*pi/180; %[rad]
phi34R_UpLimit  = 50*pi/180; %[rad]

% =========================== %
% MUSCLE-SKELETON ATTACHMENTS %
% =========================== %
                         
% hip abductor (HAB)
rHAB      =      0.06; % [m]   constant lever contribution 
phirefHAB = 10*pi/180; % [rad] reference angle at which MTU length equals 
rhoHAB    =       0.7; %       sum of lopt and lslack 

% hip adductor (HAD)
rHAD      =      0.03; % [m]   constant lever contribution 
phirefHAD = 15*pi/180; % [rad] reference angle at which MTU length equals 
rhoHAD    =         1; %       sum of lopt and lslack 
       
% --------------------------
% muscle-specific parameters
% --------------------------

% hip abductor (HAB)
FmaxHAB    =     3000; % maximum isometric force [N]
loptHAB    =     0.09; % optimum fiber length CE [m]
vmaxHAB    =       12; % maximum contraction velocity [lopt/s]
lslackHAB  =     0.07; % tendon slack length [m]

% hip adductor (HAD)
FmaxHAD    =     4500; % maximum isometric force [N]
loptHAD    =     0.10; % optimum fiber length CE [m]
vmaxHAD    =       12; % maximum contraction velocity [lopt/s]
lslackHAD  =     0.18; % tendon slack length [m]

% ========================== %
% NEURAL TRAJSMISSION DELAYS %
% ========================== %

LongLoopDelay	= 0.030;    % [s] additional to spinal reflexes
LongDelay       = 0.020/2;  % [s] ankle joint muscles
MidDelay        = 0.010/2;  % [s] knee joint muscles
ShortDelay      = 0.005/2;  % [s] hip joint muscles
MinDelay        = 0.001/2;  % [s] between neurons in the spinal cord



