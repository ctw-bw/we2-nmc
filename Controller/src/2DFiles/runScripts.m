% this script is called by nms_3Dmodel.mdl
warning('off','all') % Needed for speed (Simulink gives warnings)

walkType = 1;i_walkType = 1; 
% 1: normal walk
% 2: robust walk
% 3: Optimized walking at 0.65 m/s
% 4: Inverse Optimized walking at 0.65 m/s, general settings, with dists
% 5: Inverse Optimized walking at 1.2 m/s, general settings, with dists

%% PreNaming
CtrlPar_FP_R_List = []; CtrlPar_St_R_List = []; CtrlPar_Sw_R_List = [];
CtrlPar_Tr_R_List = []; CtrlPar_VelocityFile_List = []; 
theta0_control_List = []; maxLoad_control_List= [];

switch walkType
    case 1
        load('param_02cm');	% paramIC
        CtrlPar_VelocityFile_List = [CtrlPar_VelocityFile_List; 1.2];
    case 2
        load('param_V84_Set13AllHyb_nAnkle');     % param
        CtrlPar_VelocityFile_List = [CtrlPar_VelocityFile_List; 0.65];
    case 3
        load('param_V26_inv_nAnkle');   % paramIC
        CtrlPar_VelocityFile_List = [CtrlPar_VelocityFile_List; 1.2];
    case 4
        load('param_V35_inv_nAnkle');             % paramIC
        CtrlPar_VelocityFile_List = [CtrlPar_VelocityFile_List; 0.65];
end

    % Checking for doubles in velocity, which should be avoided
    if length(unique(CtrlPar_VelocityFile_List(:,1)))<size(CtrlPar_VelocityFile_List,1)
        error('Velocities are the same for two paramSets'); 
    end
    
    % If paramSize is 82, it means no M11 was optimized. However, a full
    % parameter set is needed for the program to run. 
    % M11 is deactivated by adding parametrs with high thresholds (10,-10)
    % and zeros for the activation gain parameters.
    if size(param,1) == 82
        param = [param(1:35);[10;0;0;0;-10;0;0;0];param(36:end)];
    end
     
    setAnkleDepInd;
    nms_MechInit;
    setCtrlParJustHipR;

%% Filling CntrlParam Lists (Used for interpolation if multiple are selected)
CtrlPar_FP_R_List = [CtrlPar_FP_R_List; CtrlPar_FP_R];    CtrlPar_St_R_List = [CtrlPar_St_R_List; CtrlPar_St_R];    CtrlPar_Sw_R_List = [CtrlPar_Sw_R_List; CtrlPar_Sw_R];
CtrlPar_Tr_R_List = [CtrlPar_Tr_R_List; CtrlPar_Tr_R];

%% Mean trunk angle and maxLoad during walking/optimization
if exist('maxLoad','var') && exist('meanTheta','var')
    theta0_control_List(i_walkType) = meanTheta;
    maxLoad_control_List(i_walkType) = maxLoad;
else
    theta0_control_List(i_walkType) = 0.1;
    maxLoad_control_List(i_walkType) = 1.2;
    disp('No Theta0 and maxLoad present in paramFile, set to standard')
end

switch walkType
    case 1
        load('param/param_V74_2D');	% paramIC
    case 2
        load('param/param_V79_2D_for');     % param
    case 3
        load('param/param_V94_2D_inv');   % paramIC
    case 4
        load('param/param_V100_2D_inv');             % paramIC
end

% Running the several Param Scripts for IID
IID_nms_MechInit;
IID_setInitPar;
IID_setCtrlPar;