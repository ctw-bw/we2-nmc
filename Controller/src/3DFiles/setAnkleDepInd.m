%% Turn Ankle Module On or Off
% This script sets the indices of multiple selectors to select the right
% channels in order to let the model work for either with/without ankle

SelectorCartPos = 1:21; SelectorCartPosSize = 21;
SelectorLOPESIn = 3:10; SelectorLOPESInSize = 8;
SelectActive = 1:8; SelectorLOPESModelPosSize = 8;
SelectorLOPESModelPosPelvisRot = 10:11;

%% Scopes
SelectorScopesLeft = 3:5; SelectorScopesSize = 8;
SelectorScopesRight = 6:8;
