%% This Script Converts A struct to a .m file
% It only allows for a two layer struct
load('LopesDevelopmentLibrary_GaitDataFixed.mat')
fid = fopen('LopesDevelopmentLibrary_GaitDataFixed.m', 'wt+');
fieldNames = fieldnames(GaitData);

% Loading fields of layer 1
for i_fields = 1:size(fieldNames,1)
    % loading fields of layer 2
    fieldNames2 = fieldnames(GaitData.(fieldNames{i_fields}));
    for i_fields2 = 1:size(fieldNames2,1)
        % Printing "field" = 
        fprintf(fid,['GaitData.',(fieldNames{i_fields}),'.',(fieldNames2{i_fields2}),' = [...\n']);
        
        %Adding Rows
        if strcmp('Walking',(fieldNames{i_fields}))
           nrRows = 10; 
        else
           nrRows = 9;
        end
        
        for i_rows = 1:nrRows
            % Copying the last row multiple times to get nrRows
            if i_rows > size(GaitData.(fieldNames{i_fields}).(fieldNames2{i_fields2}),1)
                inputRow = size(GaitData.(fieldNames{i_fields}).(fieldNames2{i_fields2}),1);
            else
                inputRow = i_rows;
            end
            
            % Printing data row per row
            fprintf(fid, '%12.5f\t',GaitData.(fieldNames{i_fields}).(fieldNames2{i_fields2})(inputRow,:));
            fprintf(fid, ';...\n');
        end
        % Closing Matrix and going to new row
        fprintf(fid, '];\n\n');
    end
end

% Adding saving line
fprintf(fid,'save(''LopesDevelopmentLibrary_GaitDataWalkingStraight.mat'',''GaitData'')');

% Closing
fclose(fid);