function GaitData = mirrorGaitData(GaitData,jointList,gaitTypeList,mirrorGaitTypeList)
%% This function mirrors the input GaitData. 
% GaitData has to be a struct with different walking types (also structs,
% IE: GaitData.Walking). Each walking type struct should contain different
% joints (IE: GaitData.Walking.LAnkleZ). This function mirrors the
% presented keyPoints inside the jointStruct in one of 2 ways: 
% 1. The keyPoints for one side are given, and mirrorored to the other
% side: LAnkleZ is copied to RAnkleZ (using mod (x + 50,100))
% 2. The keyPoints for one type of walking are mirror to the other side of
% walking: GaitData.StopLeft is copied to GaitData.StopRight

% Left is always copied to right!

% Preforming check
if length(mirrorGaitTypeList)~= length(gaitTypeList)
    error('mirrorGaitTypeList and gaitTypeList should be the same size');
end

% Mirroring data
for i_gaitType = 1:length(gaitTypeList)
    % Setting gaitType (and applying mirror settings)
    if ~mirrorGaitTypeList(i_gaitType)
        gaitType = gaitTypeList{i_gaitType};
    else
        gaitType = [gaitTypeList{i_gaitType},'Left'];
    end
    
    for i_joint = 1:length(jointList)
        %% Defining joints
        joint = jointList{i_joint};
                
        %% Applying mirror on normal walking
        if ~mirrorGaitTypeList(i_gaitType)            
            %% Setting Right Side
            % Copying KeyPoints and applying mod(x+50,100)
            GaitData.(gaitType).(['R',joint]) = GaitData.(gaitType).(['L',joint]);
            GaitData.(gaitType).(['R',joint])(:,1) = mod(GaitData.(gaitType).(['L',joint])(:,1) + 50,100);
            
            % In case of AnkleZ (Left/Right), we should mirror the keyPoints
            if strcmp('AnkleZ',joint)
                GaitData.(gaitType).(['R',joint])(:,2:3) = -GaitData.(gaitType).(['R',joint])(:,2:3);
            end
        else
            %% Applying Mirror on GaitType
            gaitTypeMirror = [gaitTypeList{i_gaitType},'Right'];
            
            % L and R
            sideList = {'L','R','L'}; %Last L only used for mirroring
            for i_side = 1:length(sideList)-1
                side = sideList{i_side}; cside = sideList{i_side+1};
                                
                % Copying KeyPoints and applying mod(x+50,100)
                GaitData.(gaitTypeMirror).([side,joint]) = GaitData.(gaitType).([cside,joint]);
                GaitData.(gaitTypeMirror).([side,joint])(:,1) = mod(GaitData.(gaitType).([cside,joint])(:,1) + 50,100);
                
                % In case of AnkleZ (Left/Right), we should mirror the keyPoints
                if strcmp('AnkleZ',joint)
                    GaitData.(gaitTypeMirror).([side,joint])(:,2:3) = -GaitData.(gaitTypeMirror).([side,joint])(:,2:3);
                end
            end 
        end
    end
end
end

