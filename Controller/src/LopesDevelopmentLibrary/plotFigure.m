close all;  
%% Plot Doll
CC = CartesianCoordinates(2:end,:,:);
Percentage = CartesianCoordinates(1,:,:);
ST = CurrentStrideTime; 
SMF = 3; 

figure; 
plot(squeeze(Percentage(1,1:end-1,1)),squeeze(CC(2,1:end-1,3)))

figure; 
for i_types = [2 1 3]
    tic; 
    i_frames = 1; 
    stepSize = mean(diff(Percentage(1,1:2,i_types))) * ((ST(i_types)*SMF)/100); %(f/% * %/s) 
    while size(Percentage(1,:,i_types),2) > i_frames                
            for i_sides = 1:2
            subplot(1,2,i_sides)
            plot3(CC(1:3:18,i_frames,i_types),CC(3:3:18,i_frames,i_types),CC(2:3:18,i_frames,i_types))
            hold on;
            plot3(CC([4 19:3:30],i_frames,i_types),CC([6 21:3:30],i_frames,i_types),CC([5 20:3:30],i_frames,i_types))
            
            if i_sides == 1
                view(0,0)
                title(['Side (',num2str(Percentage(1,i_frames,i_types)),')'])
            else
                view(90,0)  
                title('Frontal')
            end
            
            xlim([-0.6 0.6]); ylim([-0.6 0.6]); zlim([-0.9 0.3]); axis equal;
            drawnow; pause(0.01);

            hold off;     
            end
            currentTime = toc;
            i_frames = floor(currentTime / stepSize) + 1;%(s/f * s)
    end
end