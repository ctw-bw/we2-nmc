function [LAnkleX,LAnkleY,RAnkleX,RAnkleY] = angles2cc(LHF,LKF,RHF,RKF,ULL,LLL)
%% This function converts keyPoints of Hip angle and Knee Angle to AnkleX and AnkleY keypoints
% LHF = Angle Left Hip (Flexion +)
% LKF = Angle Left Knee (Flexion +)
% RHF = Angle Right Hip (Flexion +)
% RKF = Angle Right Knee (Flexion +)
% ULL = Upper leg length
% LLL = Lower leg length

%% Left Ankle X
LAnkleX(:,1) = LHF(:,1);
LAnkleX(:,2) = ULL * sin(LHF(:,2)) - LLL * sin(LKF(:,2) - LHF(:,2));
LAnkleX(:,3) = ULL * cos(LHF(:,2)).*LHF(:,3)...
    - LLL * cos(LKF(:,2) - LHF(:,2)) .* (LKF(:,3) - LHF(:,3));

%% Left Ankle Y
LAnkleY(:,1) = LHF(:,1);
LAnkleY(:,2) = (ULL + LLL) + (- ULL * cos(LHF(:,2)) - LLL * cos(LKF(:,2) - LHF(:,2)));
LAnkleY(:,3) = ULL * sin(LHF(:,2)).*LHF(:,3)...
    + LLL * sin(LKF(:,2) - LHF(:,2)) .* (LKF(:,3) - LHF(:,3));

%% Right Ankle X
RAnkleX(:,1) = RHF(:,1);
RAnkleX(:,2) = ULL * sin(RHF(:,2)) - LLL * sin(RKF(:,2) - RHF(:,2));
RAnkleX(:,3) = ULL * cos(RHF(:,2)).*RHF(:,3)...
    - LLL * cos(RKF(:,2) - RHF(:,2)) .* (RKF(:,3) - RHF(:,3));

%% Right Ankle Y
RAnkleY(:,1) = RHF(:,1);
RAnkleY(:,2) = (ULL + LLL) + (-ULL * cos(RHF(:,2)) -LLL * cos(RKF(:,2) - RHF(:,2)));
RAnkleY(:,3) = ULL * sin(RHF(:,2)).*RHF(:,3)...
    + LLL * sin(RKF(:,2) - RHF(:,2)) .* (RKF(:,3) - RHF(:,3));
