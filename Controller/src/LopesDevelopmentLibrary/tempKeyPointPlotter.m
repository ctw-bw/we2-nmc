CC = CartesianCoordinates(2:end,:,:);
Percentage = CartesianCoordinates(1,:,:);
ST = CurrentStrideTime; 

figure;
plot(squeeze(CC(14,1:end-5,1)))

figure; hold on; 
plot(squeeze(CC(13,1:end-5,1)),squeeze(CC(14,1:end-5,1)))
plot(squeeze(CC(10,1:end-5,1)),squeeze(CC(11,1:end-5,1)))
plot(squeeze(CC(7,1:end-5,1)),squeeze(CC(8,1:end-5,1)),'.','MarkerSize',20)


for i_frame = round(([1 25 60 70 80 90]*100)/105)
plot([CC(7,i_frame,1) CC(10,i_frame,1) CC(13,i_frame,1)],...
    [CC(8,i_frame,1) CC(11,i_frame,1) CC(14,i_frame,1)],'k')
plot(CC(13,i_frame,1),...
   CC(14,i_frame,1),'k.','MarkerSize',15)
end

ylabel('y (m)')
xlabel('x (m)')
axis equal
axis tight