close all;
%% Plot Doll
CC = CartesianCoordinates2';
t = (0:length(CartesianCoordinates2)-1)/1000;
SMF = 2;

figure;
tic;
i_frames = 1;
stepSize = t(2)-t(1);
while size(t,2) > i_frames
    for i_sides = 1:2
        subplot(1,2,i_sides)
        plot3(CC(1:3:18,i_frames),CC(3:3:18,i_frames),CC(2:3:18,i_frames))
        hold on;
        plot3(CC([4 19:3:30],i_frames),CC([6 21:3:30],i_frames),CC([5 20:3:30],i_frames))
        
        if i_sides == 1
            view(0,0)
            title(['Side (',num2str(t(1,i_frames)),')'])
        else
            view(90,0)
            title('Frontal')
        end
        
        xlim([-0.6 0.6]); ylim([-0.6 0.6]); zlim([-0.9 0.3]); axis equal;
        drawnow; pause(0.01);
        
        hold off;
    end
    currentTime = toc;
    i_frames = floor(currentTime / (stepSize * SMF)) + 1;
end