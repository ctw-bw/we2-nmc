% LopesDevelopmentLibraryOnPostLoadFcn.m
%
% Execute this script before compiling any Simulink model using the LopesDevelopmentLibrary.
% Use it to pre-set (configuration) variables.
d2r=@(d) d*(pi/180); % Convert degrees to radians

%% Sample time (be sure to use this in the model and not assume it will be
% the same always; we may change it to 1/1024 some day!)
LopesParams.ts = 0.001;
ts = LopesParams.ts; % Struct elements cannot be given as a parameter to embedded matlab functions, so it seems

%% Gait data
load('LopesDevelopmentLibrary_GaitDataStraightWalking.mat');
LopesParams.GaitData.StraightWalking = GaitData; clear GaitData;
load('LopesDevelopmentLibrary_GaitDataGroundClearance.mat');
LopesParams.GaitData.GroundClearance = GaitData; clear GaitData;
load('LopesDevelopmentLibrary_GaitDataGroundClearanceLeftHigh.mat');
LopesParams.GaitData.GroundClearanceLeftHigh = GaitData; clear GaitData;
load('LopesDevelopmentLibrary_GaitDataGroundClearanceRightHigh.mat');
LopesParams.GaitData.GroundClearanceRightHigh = GaitData; clear GaitData;
load('LopesDevelopmentLibrary_GaitDataStairs.mat');
LopesParams.GaitData.Stairs = GaitData; clear GaitData;
load('LopesDevelopmentLibrary_GaitDataStairsDescend.mat');
LopesParams.GaitData.StairsDescend = GaitData; clear GaitData;
load('LopesDevelopmentLibrary_GaitDataStairsDescendFront.mat');
LopesParams.GaitData.StairsDescendFront = GaitData; clear GaitData;
load('LopesDevelopmentLibrary_GaitDataRamp.mat');
LopesParams.GaitData.Ramp = GaitData; clear GaitData;
load('LopesDevelopmentLibrary_GaitDataRampDescend.mat');
LopesParams.GaitData.RampDescend = GaitData; clear GaitData;
load('LopesDevelopmentLibrary_GaitDataRampDescendFront.mat');
LopesParams.GaitData.RampDescendFront = GaitData; clear GaitData;
load('LopesDevelopmentLibrary_GaitDataSteppingStones.mat');
LopesParams.GaitData.SteppingStones = GaitData; clear GaitData;
load('LopesDevelopmentLibrary_GaitDataSideStep.mat');
LopesParams.GaitData.SideStep = GaitData; clear GaitData;
load('LopesDevelopmentLibrary_GaitDataSitToStand.mat');
LopesParams.GaitData.SitToStand = GaitData; clear GaitData;
load('LopesDevelopmentLibrary_GaitDataStandToSit.mat');
LopesParams.GaitData.StandToSit = GaitData; clear GaitData;

%% Stiffness parameters
% See the GaitGeneration|ConvertStiffness block.
LopesParams.Stiffness.Gamma = 2;
LopesParams.Stiffness.MaxJointStiffness =  [5000 20000 750 750 750 750 750 750 750 750];
LopesParams.Damping.MassFactor = [0.075,0.005,0.075,0.01,0.075,0.005,0.075,0.01];

%% Show message
disp('Executed LopesDevelopmentLibrary_LoadParameters.')
