function GaitData = addUselessRows(GaitData,jointList,gaitTypeList)
%% This function mirrors the input GaitData.

% Mirroring data
for i_gaitType = 1:length(gaitTypeList)
    for i_joint = 1:length(jointList)
        gaitType = gaitTypeList{i_gaitType};
        
        %% Defining joints
        joint = jointList{i_joint};
        
        % Amount of needed rows
        if strcmp('Walking',gaitType)
            nrRows = 10;
        else
            nrRows = 9;
        end
        
        % Amount of Rows That need to be added
        nrRowsAdd = nrRows - size(GaitData.(gaitType).(joint),1);
        GaitData.(gaitType).(joint) = [GaitData.(gaitType).(joint);...
            ones(nrRowsAdd,1)*GaitData.(gaitType).(joint)(end,:)];
    end
end
end