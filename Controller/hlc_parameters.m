% Started a file to compile controller parameters structured
clear hlc_params

hlc_params.WE.Config = 8;   %WE2: 8, WE1: 4, WEa: 2
%% Impedance ControllerParams
hlc_params.IC.MassFactor = [0.075 0.005  0.075 0.01 0.075 0.005 0.075 0.01]*2*1.1^2;
hlc_params.IC.Stiffness_Nm_rad = [750 750 750 750 750 750 750 750];
hlc_params.IC.Damping_Nms_rad = sqrt(hlc_params.IC.Stiffness_Nm_rad.*hlc_params.IC.MassFactor);
hlc_params.IC.PositionLimiterSpeed = 1;
hlc_params.IC.VelocityLimiterSpeed = 10;
hlc_params.IC.PositionBias_rad = [0.075, 0, 0, -0.2, 0.075, 0, 0, -0.2];
hlc_params.IC.PositionBiasRate = 0.05;