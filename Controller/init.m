clear
close all
clc

%% Settings

modelName = 'WE2_3D';       % Model Name
xpcModelAvailable = 0;      % If model previously has been build, this greatly increases build speed
use_3D_NMC_Model = 1;       % Set to "1" to use 3D Song model (2015) or to "0" to use 2D Geyer model (2010) 

is_offline = contains(modelName, 'Offline');

%% Prepare PATH
addpath('src/LopesDevelopmentLibrary');
addpath('src/NMC');
addpath('wr-actuator-library');

if use_3D_NMC_Model == 1
    addpath('src/3DFiles');
    if contains(path, '2DFiles')
        rmpath('src/2DFiles');
    end
elseif use_3D_NMC_Model == 0
    addpath('src/2DFiles');
    if contains(path, '3DFiles')
        rmpath('src/3DFiles');
    end
else
    error('use_3D_NMC_Model can either be 1 or 0') 
end

if is_offline
    addpath('src/OfflineDataFiles');
end

%% Run scripts
runScripts; % Inside 2D/3DFiles
LopesDevelopmentLibrary_LoadParameters;
hlc_parameters;
WE2_library_init;

%% A comment in/out section for NMC
% In library, comment in 2D/3D and comment out other part
% This if-statement is only used, if the current setup is not in line with
% the chosen option (IE we want to use the 3D model, however the 2D model
% is commented in
load_system('nmc')
if use_3D_NMC_Model == 1 && ...
        (strcmp(get_param('nmc/NMC Model/NMC 3D','commented'),'on') || strcmp(get_param('nmc/NMC Model/NMC 2D','commented'),'off'))
    set_param('nmc','Lock','off')
    set_param('nmc/NMC Model/NMC 3D','commented','off');
    set_param('nmc/NMC Model/NMC 2D','commented','on');
    set_param('nmc','Lock','on')
    save_system('nmc')
elseif use_3D_NMC_Model == 0 && ...
        (strcmp(get_param('nmc/NMC Model/NMC 3D','commented'),'off') || strcmp(get_param('nmc/NMC Model/NMC 2D','commented'),'on'))
    set_param('nmc','Lock','off')
    set_param('nmc/NMC Model/NMC 3D','commented','on');
    set_param('nmc/NMC Model/NMC 2D','commented','off');
    set_param('nmc','Lock','on')
    save_system('nmc')
end
 
%% If model is the Offline model, Also run the Offline DataPrep file
if is_offline
    StartOfflineTest;
end

%% Open system
open_system(modelName);
