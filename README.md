# WE2 Neuromuscular Controller (NMC)

This repository contains the high-level NMC for the WE2 exoskeleton. There are both the TwinCAT solution and the Simulink models.

See also:

 - [WR Actuator Library](../../../wr-actuator-library).

## Compile

 * Open `Controller/` in MATLAB
 * Run `init.m`
 * Open `WE2_3D` and compile it

## Requirements

In order to compile the model, you need the following programs and toolboxes:

 * MATLAB 2018b
 * TwinCAT TE1400 module (incl. license)
 * DSP System Toolbox
 * Simscape Electrical
 * Simulink
 * Simulink Coder

## Structure

 * `TwinCAT/` - The TwinCAT 3 solution (look for the .sln file)
 * `Controller/` - Simulink model

## Authors

 * Gijs van Oort
 * Wolfgang Rampeltshammer
